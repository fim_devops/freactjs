# build stage
FROM node:14-alpine as base
WORKDIR /app
RUN npm install -g npm
RUN chown -R node:node /app
USER node
COPY package.json package-lock.json ./

FROM base as test
ENV CI=true
RUN npm ci
COPY . /app
RUN npm run test

FROM base as build-stage
RUN npm ci --production
COPY ./ /app
RUN npm run build
#export NODE_OPTIONS=--openssl-legacy-provider && 

# production stage
FROM nginx:1.15 as production-stage
COPY --from=build-stage /app/build /usr/share/nginx/html
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]
