import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Customer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			customers: [],
			isLoaded: false,
		}
	}

	getAllCustomers() {
		fetch("http://localhost/api/")
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						customers: result,
						isLoaded: true
					});
					console.log(this.state.customers);
				},
				(error) => {
					this.setState({
						isLoaded: true
					});
				}
			)
	}

	sendPostRequest() {
		let newName = document.getElementById('name');
		let newDescription = document.getElementById('description');
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ name: newName.value, description: newDescription.value, })
		};
		fetch('http://localhost/api/', requestOptions)
		this.getAllCustomers();
	}

	render() {
		const isLoaded = this.state.isLoaded;
		if (!isLoaded) {
			this.getAllCustomers();
		}
		let customers = this.state.customers;
		return (
			<div>
				<div className="form">
					<label htmlFor="name">Name: </label>
					<input type="text" id="name" />
					<label htmlFor="description">Description: </label>
					<input type="text" id="description" />
					<button className="addBut" onClick={() => this.sendPostRequest()}>Add cusomter</button>
				</div>
				<ul>
					{customers.map(item => (
						<li key={item.id}>
							ID:{item.id} - Name: {item.name} - Description: {item.description}
						</li>
					))}
				</ul>
			</div>
		)
	}
}

ReactDOM.render(
	<Customer />,
	document.getElementById('root') || document.createElement('div')
);

export default Customer;