import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Customer from './index';

test('Check field name', () => {
  render(<Customer />);
  const linkElement = screen.getByText(/Name/i);
  expect(linkElement).toBeInTheDocument();
});
